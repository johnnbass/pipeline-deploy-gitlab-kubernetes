$("#btn-gravar").on("click", function () {
  const txtNome = $("#nome").val();
  const txtMensagem = $("#mensagem").val();

  $.ajax({
    url: "http://34.135.1.149/incluir.php",
    type: "POST",
    data: { nome: txtNome, mensagem: txtMensagem },
		beforeSend: function() {
			$("#resposta").html("Enviando...");
		}
  })
	.done(function(e) {
		$("#resposta").html("Dados salvos...");
	});
});
